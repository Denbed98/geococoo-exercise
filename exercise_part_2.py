import numpy as np
import re

'''
File containing functions for the solution of the exercise 22 of the advent of code challenge 2019.
'''

def is_bit_set_at(number, bit_index):
    '''Function that returns whether the bit of the given integer at the given index is set or not.
    
    Args:
        number (Numeric): The number to check the bits for.
        bit_index (Numeric): The index of the bit to check.
    
    Returns:
        Boolean: Is the bit of the number set at the index.
    '''
    return ((number>>bit_index)&1)==1

def get_default_representation():
    '''Returns a default representation for a deck of cards.
    
    Returns:
        np.ndarray: A (2x1) matrix where the first number is the m and the second number the b for the function with which one can determine the value of a card at a position.
    '''
    representation = np.zeros((2,1), dtype=np.int64)
    representation[0,0] = 1
    representation[1,0] = 0
    
    return representation

def get_neutral_transformation():
    '''Returns the neutral transformation element which is a identity matrix with the shape (2x2).
    
    Returns:
        np.ndarray: Neutral element for the transformation of representations.
    '''
    return np.identity(2, dtype=np.int64)

def extended_euclidian_algorithm(a,b):
    '''The extended euclidian algorithm which returns the greatest common divisor as well as two numbers s and t which solve the following equation gcd(a,b) = s*a + t*b. 
    
    Args:
        a (Numeric): The first number to get the gcd for.
        b (Numeric): The second number to get the gcd for.
    
    Returns:
        Numeric: The greatest common divisor for the two given numbers.
        Numeric: The s from the equation gcd(a,b) = s*a + t*b.
        Numeric: The t from the equation gcd(a,b) = s*a + t*b.
    '''
    if (b==0):
        return (a,1,0)
    
    (d_dash,s_dash,t_dash) = extended_euclidian_algorithm(b,a%b)
    (d,s,t) = (d_dash, t_dash, s_dash-((a//b)*t_dash))
    
    return (d,s,t)

def deal_into_new_stack_transformation():
    '''Returns the transformation matrix for the operation "deal into new stack".
    
    Returns:
        np.ndarray: a (2x2) matrix which will inverse the order of a deck represented by a representation.
    '''
    mat = np.zeros((2,2), dtype=np.int64)
    
    mat[0,0] = -1
    mat[0,1] = 0
    mat[1,0] = -1
    mat[1,1] = 1

    return mat

def cut_n_cards_transformation(n):
    '''Returns the transformation matrix for the operation "cut n cards".
    
    Args:
        n (Numeric): The amount of cards to cut from the beginning (or end if negative).
        
    Returns:
        np.ndarray: a (2x2) matrix which will switch the first n (or last -n if negative) cards of a deck represented by a representation with the rest of the cards.
    '''
    mat = np.zeros((2,2), dtype=np.int64)
    
    mat[0,0] = 1
    mat[0,1] = 0
    mat[1,0] = n
    mat[1,1] = 1

    return mat

def deal_with_increment_n_transformation(n, size):
    '''Returns the transformation matrix for the operation "deal with increment n".
    
    Args:
        n (Numeric): The distance to put between cards in the new order which have previously been next to one another.
        size (Numeric): The size of the deck the operation is going to be applied to.
        
    Returns:
        np.ndarray: a (2x2) matrix which will order the cards in such a way that cards previously next to one another end up n places apart.
    '''
    mat = np.zeros((2,2), dtype=np.int64)
    
    mat[0,0] = extended_euclidian_algorithm(size, n)[2]
    mat[0,1] = 0
    mat[1,0] = 0
    mat[1,1] = 1

    return mat

def chain_transformations(transformation_1, transformation_2, size):
    '''Chains two transformation operations together returning a transformation which when applied to a deck representation results in the same change as if both transformations had been applied separately.
    
    Args:
        transformation_1 (np.ndarray): The transformation that would be applied first.
        transformation_2 (np.ndarray): The transformation that would be applied second.
        size (Numeric): The size of the deck the transformation is supposed to be applied to.
    Returns:
        np.ndarray: a (2x2) matrix which has the effect of both transformations put into this function, when applied to a deck representation.
    '''
    #Overflow safe matrix multiplication by using python-native integer types:
    mat = np.zeros((2,2), dtype=np.int64)
    
    for i in range(2):
        for j in range(2):
            current_number = 0
            for k in range(2):
                    number_1 =  transformation_2[i,k].item()
                    number_2 = transformation_1[k,j].item()
                    current_number += (number_1*number_2)
            current_number %= size
            mat[i,j] = current_number

    return mat

def execute_transformation_on_representation(representation, transformation):
    '''Executes the given transformation on the given representation of a deck.
    
    Args:
        representation (np.ndarray): The representation of the deck that is supposed to be transformed.
        transformation (np.ndarray): The transformation to apply to the given deck representation.
        
    Returns:
        np.ndarray: The transformed deck representation.
    '''
    return (transformation@representation)

def get_card_at_position(representation, position, size):
    '''Returns the value of the card at the given position of the given deck with the given size.
    
    Args:
        representation (np.ndarray): The deck representation.
        position (Numeric): The position in the deck.
        size (Numeric): The size of the deck.
        
    Returns:
        Numeric: The value of the card at the given position for the given deck representation and size.
    '''
    m = representation[0]
    b = representation[1]

    return (m*position + b)%size

def transformation_for_file_orders(file, size):
    """Function that executes a set of text commands on the given deck.
    
    Args:
        deck (np.ndarray): An one dimensional ndarray with dtype=np.uint64 which represents a deck of numbered cards.
        orders (string): A string which describes a list of operations that are to be performed on the deck.
                         "deal into new stack" Inverts the order of the deck
                         "deal with increment *" Reorders the cards to be * positions apart of each other if they were next to each other beforehand.
                         "cut *" Cuts * cards from the beginning (positive *) or end (negative *) of the deck and switches them with the rest of the deck.
    
    Returns:
        np.ndarray: The state of the deck after the transformations.
    """
    
    #Loads the orders from the file:
    f = open(file, "r")
    
    content = f.read()
    f.close()
    
    #Splits the command string by lines:
    lines = content.split("\n")
    
    #Neutral transformation element:
    mat = get_neutral_transformation()
    
    #Creates the regular expressions:
    deal_into_new_stack_expr = re.compile("deal into new stack")
    cut_n_cards_expr = re.compile("(cut )[-]{0,1}[1-9]{1}[0-9]{0,}")
    deal_with_increment_n_expr = re.compile("(deal with increment )[-]{0,1}[1-9]{1}[0-9]{0,}")
    
    #Iterates over the lines and executes each command on the deck:
    for i in range(0, len(lines)):
        line = lines[i]
        words = line.split(" ")
        last_word = words[len(words)-1]
        number = None
        transform_mat = None
        
        #Looks for the matchings in the line:
        deal_into_new_stack_result = deal_into_new_stack_expr.match(line)
        cut_n_cards_result = cut_n_cards_expr.match(line)
        deal_with_increment_n_result = deal_with_increment_n_expr.match(line)
        
        #Checks whether the last word is a number which would indicate a "cut" or "deal with increment" command:
        if deal_into_new_stack_result!=None and deal_into_new_stack_result.group()==line:
            transform_mat = deal_into_new_stack_transformation()
            
        elif cut_n_cards_result!=None and cut_n_cards_result.group()==line:
            number = int(last_word)
            transform_mat = cut_n_cards_transformation(number)
            
        elif deal_with_increment_n_result!=None and deal_with_increment_n_result.group()==line:
            number = int(last_word)
            transform_mat = deal_with_increment_n_transformation(number, size)
            
        else:
            raise Exception("Fehler: Der Inhalt der Datei ist ungültig!")
        
        #Chains the transformations to a new one:
        mat = chain_transformations(mat, transform_mat, size)
        
    return mat

def main_aufg1():
    '''Solves the first part of the exercise in a more efficient way, by using the representations instead of the actual deck.
    Prints the position of the card with the value 2019.
    '''
    #Settings for the deck:
    size = 10007
    card_to_find = 2019
    
    #Creates an initial default representation of a deck:
    representation = np.zeros((2,1), dtype=np.int64)
    representation[0,0] = 1
    representation[1,0] = 0
    
    #Determines the transformation for all of the instructions from the given file:
    transformation = transformation_for_file_orders("instructions.txt", size)
    
    #Transforms the representation:
    representation = execute_transformation_on_representation(representation, transformation)
    
    for i in range(size):
        if (get_card_at_position(representation, i, size)==card_to_find):
            print(i)
            

 
def main_aufg2():
    '''Solves the second part of the exercise.
    Prints the value of the card at the position 2020.
    '''
    #Settings for the deck:
    instruction_file = "instructions.txt"
    
    bit_length = 64
    size = 119315717514047
    number_of_instruction_executions = 101741582076661
    position_to_check = 2020
    
    #Creates an initial default representation of a deck:
    representation = get_default_representation()
    
    #Determines the transformation for all of the instructions from the given file:
    transformation = transformation_for_file_orders(instruction_file, size)
    
    #Calculates the transformation for each power of 2 up to 64 (bit-length of int64) and stores the result in an an array:
    transformation_array = np.empty((bit_length), dtype=np.object)
    
    #Initializes the first spot with the transformation times 1:
    transformation_array[0] = transformation
    
    #All the coming transformations are a chaining of a pair of the prior transformation:
    for i in range(1,bit_length):
        transformation_array[i] = chain_transformations(transformation_array[i-1], transformation_array[i-1], size)
    
    #Creates a new neutral transformation:
    big_transformation = get_neutral_transformation()
    
    #Goes over the bits of the number_of_instruction_executions and chains the transformation of the according transformation_array
    #element to the big_transformation if the bit is set:
    for i in range(bit_length):
        if is_bit_set_at(number_of_instruction_executions, i):
            big_transformation = chain_transformations(big_transformation, transformation_array[i], size)
            
    #Executes the big transformation on the representation:
    representation = execute_transformation_on_representation(representation, big_transformation)
    
    #Prints out the card at the given position after the transformations are done:
    print(get_card_at_position(representation, position_to_check, size))

#Executes the main function and thereby starts the application:
try:
    main_aufg2()
except Exception as err:
    print(str(err))

