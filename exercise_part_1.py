'''
Created on Jan 29, 2020

@author: Dennis
'''

import numpy as np
from _overlapped import NULL

def dealIntoNewStack(deck):
    """Function that inverts the order of the cards in the given deck.
    
    Args:
        deck (np.ndarray): An one dimensional ndarray with dtype=np.uint64 which represents a deck of numbered cards.
    Returns:
        np.ndarray: An inverted version of the given deck.
    """
    return deck[::-1]

def cutNCards(deck, n):
    """Function that cuts n cards from the beginning (positive n) or end (negative n) of the given deck and switches it around with
    the rest of the deck.
    
    Args:
        deck (np.ndarray): An one dimensional ndarray with dtype=np.uint64 which represents a deck of numbered cards.
        n (int): A number which determines the position at which the deck is supposed to be cut.
                 A positive number indicates that the cards are going to be cut from the beginning whereas 
                 a negative one indicates a cut from the end.
        
    Returns:
        np.ndarray: The cut version of the given deck.
    """
    returnDeck = np.zeros(deck.shape, dtype=np.uint64)
    
    if n>0:
        returnDeck[-n:] = deck[:n]
        returnDeck[:-n] = deck[n:]
    elif n<0:
        returnDeck[:-n] = deck[n:]
        returnDeck[-n:] = deck[:n]
    else:
        returnDeck[:] = deck[:]
    
    return returnDeck

def dealWithIncrementN(deck, n):
    """Function that orders the cards of the given deck in such a way that the cards of the old order get placed n positions after one another.
    
    Args:
        deck (np.ndarray): An one dimensional ndarray with dtype=np.uint64 which represents a deck of numbered cards.
        n (int): A number which determines the distance between each card that was next to the other before.
        
    Returns:
        np.ndarray: The reordered version of the deck.
    """
    size = deck.shape[0]
    returnDeck = np.zeros(deck.shape, dtype=np.uint64)

    index = 0
    
    for i in range(0, size):
        returnDeck[index] = deck[i]
        index = (index+n)%size

    return returnDeck

def readStringFromFile(file):
    """Function that retrieves the content of a file and returns it.
    
    Args:
        file (string): The path of the file.
        
    Returns:
        string: The content of the file as a string.
    """
    f = open(file, "r")
    
    content = f.read()
    f.close()
    
    return content

def executeOrdersOnDeck(deck, orders):
    """Function that executes a set of text commands on the given deck.
    
    Args:
        deck (np.ndarray): An one dimensional ndarray with dtype=np.uint64 which represents a deck of numbered cards.
        orders (string): A string which describes a list of operations that are to be performed on the deck.
                         "deal into new stack" Inverts the order of the deck
                         "deal with increment *" Reorders the cards to be * positions apart of each other if they were next to each other beforehand.
                         "cut *" Cuts * cards from the beginning (positive *) or end (negative *) of the deck and switches them with the rest of the deck.
    
    Returns:
        np.ndarray: The state of the deck after the transformations.
    """
    
    #Copies the deck:
    returnDeck = deck[:]
    
    #Splits the command string by lines:
    lines = orders.split("\n")
    
    #Iterates over the lines and executes each command on the deck:
    for i in range(0, len(lines)):
        line = lines[i]
        words = line.split(" ")
        lastWord = words[len(words)-1]
        number = NULL
        
        #Checks whether the last word is a number which would indicate a "cut" or "deal with increment" command:
        if lastWord[0].isalpha() == False:
            number = int(lastWord)
            
            if words[0]=="cut":
                #Cuts the deck:
                returnDeck = cutNCards(returnDeck, number)
            elif words[0]=="deal":
                #Deals the deck with increment:
                returnDeck = dealWithIncrementN(returnDeck, number)
        else:
            #Inverts the decks order:
            returnDeck = dealIntoNewStack(returnDeck)
            
    return returnDeck

if __name__ == '__main__':
    #Creates the deck:
    deck = np.arange(10007, dtype=np.uint64)
    #Loads the orders from a file and executes them:
    deck = executeOrdersOnDeck(deck, readStringFromFile("instructions.txt"))
    
    #Prints the index of the card with number 2019:
    print(np.where(deck==2019)[0][0])